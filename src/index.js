import React from 'react'
import ReactDom from 'react-dom'

import Navbar from './components/navbar/navbar'
import Hero from './components/hero/hero'
import Works from './components/works/works'
import Skills from './components/skills/skills'
import Footer from './components/footer/footer'
import Contact from './components/contact/contact'

const App = () => {
  return (
    <div>
      <Navbar />
      <div className="h-full">
        <Hero />
        <Works />
        <Skills />
        <Contact />
        <Footer />
      </div>


    </div>
  )
}

ReactDom.render(<App />, document.querySelector('#root'))