import React, { useState, useEffect } from 'react'
import { Link, animateScroll as scroll } from 'react-scroll'
import resume from './Vijeth-resume.pdf'

const Navbar = () => {
  const [toggle, setToggle] = useState(false)
  const [navpos, setnavpos] = useState(true)

  const toggleBtn = () => {
    setToggle(!toggle)
  }

  var prevScrollpos = window.pageYOffset;
  window.onscroll = function () {
    var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
      setnavpos(true)
    } else {
      setnavpos(false)
    }
    prevScrollpos = currentScrollPos;
  }

  useEffect(() => {
    toggleBtn
  })
  return (
    <div className={`w-full bg-white px-2 sm:px-2 md:px-6 lg:px-20 fixed ${navpos ? 'top-0' : 'hidden'}`}>

      <div className="relative bg-white">
        <div className="max-w-7xl mx-auto px-4 sm:px-6">
          <div className="flex justify-between items-center border-b-2 border-gray-100 py-4 md:justify-start md:space-x-10">
            <div className="lg:w-0 flex flex-row items-center cursor-pointer transition ease-in duration-200 text-black hover:text-teal-700">
              <span className="p-1">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-cpu"><rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect><rect x="9" y="9" width="6" height="6"></rect><line x1="9" y1="1" x2="9" y2="4"></line><line x1="15" y1="1" x2="15" y2="4"></line><line x1="9" y1="20" x2="9" y2="23"></line><line x1="15" y1="20" x2="15" y2="23"></line><line x1="20" y1="9" x2="23" y2="9"></line><line x1="20" y1="14" x2="23" y2="14"></line><line x1="1" y1="9" x2="4" y2="9"></line><line x1="1" y1="14" x2="4" y2="14"></line></svg>
              </span>
              <span>
                <Link
                  to="home" smooth={`true`} duration={1000}
                  className="flex relative items-center font-bold text-3xl">
                  <span className="">HOME</span>
                </Link>
              </span>
            </div>
            <div className="-mr-2 -my-2 md:hidden">
              <button
                type="button"
                aria-label="open"
                className="inline-flex items-center justify-center p-2 rounded-md text-gray-600 hover:text-gray-700 hover:bg-gray-200 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                onClick={toggleBtn}
              >
                <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </button>
            </div>

            <div className="hidden md:flex items-center justify-end space-x-8 md:flex-1 lg:w-0 font-medium text-base">
              <Link
                to="work"
                className="mx-4 transition ease-in duration-200 border-white border-b-2 hover:border-teal-700 cursor-pointer hover:text-teal-700"
                smooth={`true`} duration={1000}
              >
                My Work
              </Link>
              <Link
                to="contact"
                className="mx-4 transition ease-in duration-200 border-white border-b-2 hover:border-teal-700 cursor-pointer hover:text-teal-700"
                smooth={`true`} duration={1000}
              >
                Contact Me
              </Link>
              <a
                href={resume}
                target='_blank'
                className="mx-4 transition ease-in duration-200 border-white border-b-2 hover:border-teal-700 cursor-pointer hover:text-teal-700"
                smooth={`true`} duration={1000}
              >
                Resume
              </a>
            </div>
          </div>
        </div>

        <div className={`absolute top-0 inset-x-0 p-2 transition transform origin-top-left ease-in duration-200 ${toggle ? '' : 'hidden'}`}>
          <div className="rounded-lg shadow-lg">
            <div className="rounded-lg shadow-xs bg-white divide-y-2 divide-gray-50">

              <div className="px-2 py-2 space-y-6">
                <div className="flex items-center justify-between">
                  <div className="transition ease-in duration-200 font-bold text-3xl cursor-pointer hover:text-teal-700">
                    <div className="lg:w-0 flex flex-row items-center cursor-pointer transition ease-in duration-200 text-black hover:text-teal-700">
                      <span className="p-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-cpu"><rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect><rect x="9" y="9" width="6" height="6"></rect><line x1="9" y1="1" x2="9" y2="4"></line><line x1="15" y1="1" x2="15" y2="4"></line><line x1="9" y1="20" x2="9" y2="23"></line><line x1="15" y1="20" x2="15" y2="23"></line><line x1="20" y1="9" x2="23" y2="9"></line><line x1="20" y1="14" x2="23" y2="14"></line><line x1="1" y1="9" x2="4" y2="9"></line><line x1="1" y1="14" x2="4" y2="14"></line></svg>
                      </span>
                      <span>
                        <Link
                          to="home" smooth={`true`} duration={1000}
                          className="flex relative items-center">
                          <span className="">HOME</span>
                        </Link>
                      </span>
                    </div>
                  </div>
                  <div className="">
                    <button
                      type="button"
                      aria-label="close"
                      className="inline-flex items-center text-xl font-bold justify-center py-1 px-3 rounded-md text-gray-600 hover:text-gray-700 hover:bg-gray-200 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                      onClick={toggleBtn}
                    >
                      X
                    </button>
                  </div>

                </div>
              </div>

              {
                toggle ?
                  <div className="py-6 px-5 space-y-6">
                    <div className="flex flex-col justify-end  space-y-6 font-medium text-base">
                      <Link
                        to="work"
                        className="w-auto mx-4 transition ease-in duration-200 border-white border-b-2 hover:border-teal-700 cursor-pointer hover:text-teal-700"
                        smooth={`true`} duration={1000} onClick={() => toggleBtn()}
                      >
                        My Work
                      </Link>
                      <Link
                        to="contact"
                        className="w-auto mx-4 transition ease-in duration-200 border-white border-b-2 hover:border-teal-700 cursor-pointer hover:text-teal-700"
                        smooth={`true`} duration={1000} onClick={() => toggleBtn()}
                      >
                        Contact Me
                      </Link>
                      <a
                        href={resume}
                        target='_blank'
                        className="w-auto mx-4 transition ease-in duration-200 border-white border-b-2 hover:border-teal-700 cursor-pointer hover:text-teal-700"
                        smooth={`true`} duration={1000} onClick={() => toggleBtn()}
                      >
                        Resume
                      </a>
                    </div>
                  </div>
                  : null
              }

            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar