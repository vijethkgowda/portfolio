import React from 'react'
import { motion } from "framer-motion"
import linkedIn from '../../../public/image/linkedIn.png'
import github from '../../../public/image/github.png'
import gitlab from '../../../public/image/gitlab.png'
import twitter from '../../../public/image/twitter.png'

const Social = () => {
  return (
    <div className="flex flex-wrap justify-center mt-10 sm:mt-12 md:mt-16 lg:mt-20">

      <motion.a
        whileHover={{ scale: 1.1 }}
        whileTap={{ scale: 0.9 }}
        href="https://www.linkedin.com/in/vijeth-k-476539152/"
        className="flex justify-center items-center w-32 h-32 py-6 m-2 bg-teal-700 shadow-2xl rounded-lg"
        rel="noreferrer" target="_blank">
        <div className="flex flex-col justify-center items-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 24 24" fill="none" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-linkedin"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></svg>
          <span className="text-white font-bold">LinkedIn</span>
        </div>
      </motion.a>

      <motion.a
        whileHover={{ scale: 1.1 }}
        whileTap={{ scale: 0.9 }}
        href="https://github.com/VijethKGowda"
        className="flex justify-center items-center w-32 h-32 py-6 m-2 bg-teal-700 shadow-2xl rounded-lg"
        rel="noreferrer" target="_blank">
        <div className="flex flex-col justify-center items-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 24 24" fill="none" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-github"><path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path></svg>
          <span className="text-white font-bold">Github</span>
        </div>
      </motion.a>

      <motion.a
        whileHover={{ scale: 1.1 }}
        whileTap={{ scale: 0.9 }}
        href="https://gitlab.com/vijethkgowda"
        className="flex justify-center items-center w-32 h-32 py-6 m-2 bg-teal-700 shadow-2xl rounded-lg"
        rel="noreferrer" target="_blank">
        <div className="flex flex-col justify-center items-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 24 24" fill="none" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-gitlab"><path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z"></path></svg>
          <span className="text-white font-bold">GitLab</span>
        </div>
      </motion.a>

      <motion.a
        whileHover={{ scale: 1.1 }}
        whileTap={{ scale: 0.9 }}
        href="https://twitter.com/vijethkgowda"
        className="flex justify-center items-center w-32 h-32 py-6 m-2 bg-teal-700 shadow-2xl rounded-lg" rel="noreferrer" target="_blank">
        <div className="flex flex-col justify-center items-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 24 24" fill="none" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>
          <span className="text-white font-bold">Twitter</span>
        </div>
      </motion.a>
    </div>
  )
}

export default Social