import React from 'react'
import ReactLogo from '../../../public/image/reactLogo.png'
import JavascriptLogo from '../../../public/image/javascriptLogo.png'
import HtmlLogo from '../../../public/image/htmlLogo.png'
import NodeLogo from '../../../public/image/nodeLogo.png'

const Skills = () => {

  return (
    <>
      <div id="skills" className="mt-10 sm:mt-12 md:mt-16 lg:mt-20">
        <div className="flex flex-col justify-center text-center pb-4 pt-4 sm:pt-4 md:pt-16 lg:pt-0 w-1/2 mx-auto mt-0 lg:w-1/2 text-2xl sm:text-2xl md:text-4xl lg:text-5xl font-bold">
          <div className="text-teal-700">Skills
          <span className="text-gray-900"> & Education</span>
            <span className="text-red-600">.</span>
          </div>
          <div className="text-gray-900 text-base sm:text-base md:text-xl lg:text-xl">
            Skills &gt; Marks.
        </div>
        </div>

        <div className="flex flex-col px-2 flex-grow sm:flex-col md:flex-col lg:flex-row justify-between mb-4 w-full sm:w-full md:w-10/12 lg:w-9/12 mx-auto">
          <div className="w-full bg-gray-100 mx-1 rounded-lg mt-4 ">
            <div className="mt-2 mx-3 font-bold text-2xl text-gray-800">
              Skills
            </div>
            <div className="flex flex-col w-full">
              <div className="flex flex-row">
                <div className="my-6 mx-2 w-1/4">
                  <img src={ReactLogo} alt="React" width="110" height="100"></img>
                </div>
                <div className="my-4 mx-2 font-semibold text-xl text-teal-700 w-3/4">
                  React.Js
                  <p className="text-sm font-semibold text-gray-700">
                    If you are searching for a skilled React.js developer, I am right here to contact me through the mail.
                  </p>
                </div>
              </div>

              <div className="flex flex-row">
                <div className="my-6 mx-2 w-1/4">
                  <img src={JavascriptLogo} alt="Javascript" width="110" height="100"></img>
                </div>
                <div className="my-4 mx-2 font-semibold text-xl text-teal-700 w-3/4">
                  JavaScript
                  <p className="text-sm font-semibold text-gray-700">
                    Even though I started working on JavaScript earlier, then I started working on React.js, but still, I can pick up Where I left.
                  </p>
                </div>
              </div>

              <div className="flex flex-row">
                <div className="my-6 mx-2 w-1/4">
                  <img src={HtmlLogo} alt="Html" width="120" height="100"></img>
                </div>
                <div className="my-4 mx-2 font-semibold text-xl text-teal-700 w-3/4">
                  HTML
                  <p className="text-sm font-semibold text-gray-700">
                    I write HTML and CSS code for fun, so it makes me good at it.
                  </p>
                </div>
              </div>

              <div className="flex flex-row">
                <div className="my-6 mx-2 w-1/4">
                  <img src={NodeLogo} alt="Node" width="120" height="100"></img>
                </div>
                <div className="my-4 mx-2 font-semibold text-xl text-teal-700 w-3/4">
                  Node.JS
                  <p className="text-sm font-semibold text-gray-700">
                    I am still learning Node.Js, but I can work on a node if no one is available.
                  </p>
                </div>
              </div>

            </div>
          </div>

          <div className="w-full bg-gray-100 mx-1 rounded-lg mt-4">
            <div className="mt-2 mx-3 font-bold text-2xl text-gray-800">
              Education
            </div>
            <div className="mt-4 mx-6">
              <p className="text-gray-700 pt-2 text-base font-semibold">
                2020 batch
              </p>
              <p className="text-xl text-gray-900 pb-2 font-bold">
                Dayananda Sagar College Of Engineering, Bengaluru <span className="text-teal-700">MCA</span>.
              </p>
              <p className="text-gray-700 text-sm pb-2 font-semibold">
                Steganography application in android during 5<sup>th</sup> semester project and E-Learning website during 6<sup>th</sup> semester.
              </p>
            </div>

            <div className="mt-4 mx-6">
              <p className="text-gray-700 text-sm pt-2 font-semibold">
                2017 batch
              </p>
              <p className="text-xl text-gray-900 pb-2 font-bold">
                Bandarkar’s Art’s & Science College, Kundapura <span className="text-teal-700">BCA</span>.
              </p>
              <p className="text-gray-700 text-sm pb-2 font-semibold">
              I developed an Integrated Library Management System for the College Library. 
              Also, Document classification software for the 6<sup>th</sup> semester project
              </p>
            </div>
          </div>

        </div>
      </div>
    </>
  )
}

export default Skills