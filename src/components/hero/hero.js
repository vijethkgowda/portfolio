import React from 'react'
import HeroBg from '../../../public/image/HeroBg'
import { Link } from 'react-scroll'

const Hero = () => {
  return (
    <div id="home" className="mt-16 mb-2 sm:mb-16 md:mb-16">
      <div className="flex flex-col lg:flex-row justify-center sm:items-center md:items-center lg:items-start w-full sm:w-full md:w-11/12 lg:w-10/12 mr-auto ml-auto">
        <div className="w-full py-8 lg:py-20 lg:w-1/2 text-2xl sm:text-2xl md:text-4xl lg:text-5xl font-bold">
          <p className="text-gray-900 py-2 pl-8 sm:pl-8 md:pl-16 lg:pl-20">He
            <span className="text-teal-700">y</span>
            <span className="text-red-600">,</span>
          </p>
          <p className="text-gray-900 py-2 text-2xl sm:text-2xl md:text-4xl lg:text-5xl pl-8 sm:pl-8 md:pl-16 lg:pl-20">
            I'm
          <span className="text-teal-700 text-2xl sm:text-2xl md:text-4xl lg:text-5xl pl-2">Vijeth</span>
          </p>
          <p className="text-gray-700 font-medium text-base sm:text-base md:text-xl lg:text-2xl pl-8 sm:pl-8 md:pl-16 lg:pl-20">
            I'm a web designer / developer based in Mangalore, India. I have a passion for web developing and love to create for web and mobile devices.
          </p>
          <div className="w-auto ml-2 sm:ml-8 md:ml-16 lg:ml-20 my-6">
            <span className="cursor-pointer mx-2">
              <Link type='button'
                to="work" smooth={`true`} duration={1000}
                className="bg-teal-700 text-base border-2 hover:bg-teal-800 rounded-lg text-white py-4 px-8 font-semibold">
                My works
            </Link>
            </span>
            <span className="cursor-pointer mx-2">
              <Link type='button'
                to="contact" smooth={`true`} duration={1000}
                className="bg-gray-300 border-2 text-base hover:bg-gray-400 rounded-lg py-4 px-8 font-semibold courser-pointer">
                Drop a mail
            </Link>
            </span>
          </div>
        </div>
        <div className="-mt-10 my-0 sm:my-10 md:my-16 lg:my-20 w-full sm:w-full md:w-10/12 lg:w-1/2 h-96 flex items-center justify-center">
          <HeroBg />
        </div>
      </div>
    </div>
  )
}

export default Hero
