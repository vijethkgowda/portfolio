import React from 'react'
import { Link } from 'react-scroll'

const Footer = () => {

  return (
    <>
      <div className="w-full bottom-0 bg-gray-100">
        <div className="flex flex-row justify-between py-2 px-2 sm:px-2 md:px-6 lg:px-20 text-center">
          <Link to="home"
            smooth={`true`} duration={1000}
            className="flex transition ease-in duration-200 font-bold text-3xl cursor-pointer hover:text-teal-700">
            <span className="px-2">Top</span>
          </Link>
          <div className="py-2 px-4 font-semibold text-base text-gray-700">© 2020 Vijeth K Gowda. All Rights Reserved.</div>
        </div>
      </div>
    </>
  )
}

export default Footer