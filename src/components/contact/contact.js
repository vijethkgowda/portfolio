import React, { useState } from "react";
import * as emailjs from 'emailjs-com';

const contact = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')

  const send = () => {
    event.preventDefault();
    const templateParams = {
      from_name: name,
      from_email: email,
      to_name: 'K Vijeth',
      message: message,
    };
    emailjs.send(
      'service_msr5tli',
      'template_hirrhsi',
      templateParams,
      'user_2mB1K0IUWHhBtU2yj9T8i'
    )
      .then((result) => {
        alert(`E-Mail sent successfully 
          code: ${result}
          `);
      }, (error) => {
        alert(`Some error occurred try again later
          code: ${error}
        `);
      });
  }

  return (
    <div className='relative px-6' id='contact'>
      <div className='max-w-screen-xl mx-auto py-20 lg:py-24'>
        <div className='p-10 sm:p-12 md:p-16 text-gray-700 rounded-lg relative'>
          <div tw="mx-auto max-w-4xl">
            <h2 className='text-3xl sm:text-4xl font-bold text-gray-900'>Contact Me</h2>
            <form className='mt-4'>
              <div className='flex flex-col sm:flex-row justify-between'>
                <div className='sm:w-5/12 flex flex-col'>
                  <div className='relative py-5 mt-6'>
                    <label
                      className='absolute top-0 left-0 tracking-wide font-semibold text-md text-gray-700'
                      htmlFor="name">
                      Your Name
                      </label>
                    <input
                      className='w-full text-gray-700 text-base font-medium tracking-wide border-2 border-teal-700 rounded-lg py-2 px-6 my-2 focus:border-pink-400 focus:outline-none transition duration-200'
                      id="name" type="text" name="name" placeholder="Eg: Tom Hanks"
                      onChange={e => setName(e.target.value)}
                    />
                  </div>
                  <div className='relative py-5 mt-6'>
                    <label
                      className='absolute top-0 left-0 tracking-wide font-semibold text-md text-gray-700'
                      htmlFor="email">
                      Your Email Address
                      </label>
                    <input
                      className='w-full text-gray-700 text-base font-medium tracking-wide border-2 border-teal-700 rounded-lg py-2 px-6 my-2 focus:border-pink-400 focus:outline-none transition duration-200'
                      id="email" type="email" name="email" placeholder="Eg: Tommyhanky@gmail.com"
                      onChange={e => setEmail(e.target.value)}
                    />
                  </div>
                </div>
                <div className='sm:w-5/12 flex flex-col'>
                  <div className='flex-1 relative py-5 mt-6'>
                    <label
                      className='absolute top-0 left-0 tracking-wide font-semibold text-md text-gray-700'
                      htmlFor="message">
                      Your Message
                      </label>
                    <textarea
                      className='h-24 my-2 sm:h-full resize-none w-full bg-transparent text-gray-700 text-base font-medium tracking-wide border-2 border-teal-700 rounded-lg py-2 px-6 focus:border-pink-400 focus:outline-none transition duration-200'
                      id="message" name="message" placeholder="Eg: Hey, Vijeth I would like to talk to you "
                      onChange={e => setMessage(e.target.value)}
                    />
                  </div>
                </div>
              </div>
              <div className='flex flex-row justify-between'>
                <button
                  className='w-full sm:w-32 mt-6 py-3 bg-teal-700 text-gray-100 border-2 rounded-lg tracking-wide shadow-lg font-semibold text-base transition duration-300 transform focus:outline-none focus:shadow-outline hover:bg-teal-800 hover:text-teal-700-700 focus:-translate-y-px'
                  type="submit"
                  value="Submit"
                  onClick={send}
                >Submit</button>
                <a
                  className='mt-6 py-3 px-2 text-blue-700 tracking-wide font-semibold text-base hover:text-teal-700-700'
                  href="mailto:vijethkgowda@gmail.com">
                  vijethkgowda@gmail.com
                   </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default contact
