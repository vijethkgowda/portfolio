import * as React from "react";
import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { v4 as uuidv4 } from 'uuid';

const Accordion = ({ i, expanded, setExpanded, title, content }) => {
  const isOpen = i === expanded;

  // By using `AnimatePresence` to mount and unmount the contents, we can animate
  // them in and out while also only rendering the contents of open accordions
  return (
    <div className="w-full sm:w-full md:w-5/6 lg:w-7/12 mx-auto shadow-2xl">
      <motion.header
        className="flex justify-between py-3 px-3 sm:px-3 md:px-4 text-white text-base sm:text-base md:text-lg font-semibold hover:bg-teal-800 rounded-lg"
        initial={false}
        animate={{ backgroundColor: isOpen ? "#285e61" : "#2c7a7b" }}
        onClick={() => setExpanded(isOpen ? false : i)}
      >
        <span>{title}</span> <span className={`px-4 text-center text-xl ${ isOpen ? 'transition ease-in-out duration-500 transform rotate-45' : 'transition ease-in-out duration-500'}`}>+</span>
      </motion.header>
      <AnimatePresence initial={false}>
        {isOpen && (
          <motion.section
            className="mt-2 rounded-lg"
            key={i}
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: "auto" },
              collapsed: { opacity: 0, height: 0 }
            }}
            transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
          >
            <p className="p-4 rounded-lg text-base font-semibold text-gray-700">
              {content}
            </p>
          </motion.section>
        )}
      </AnimatePresence>
    </div>
  );
};

export const Example = () => {
  // This approach is if you only want max one section open at a time. If you want multiple
  // sections to potentially be open simultaneously, they can all be given their own `useState`.<false | number>
  const [expanded, setExpanded] = useState();

  return accordionIds.map(i => (
    <Accordion i={i.id} expanded={expanded} setExpanded={setExpanded} title={i.title} content={i.content} />
  ));
};


const accordionIds = [
  {
    id: uuidv4(),
    title: "Document Classification using the k-nearest neighbor algorithm ",
    content: "Document Classification is a software where it scans a document and shows the genre of that document, this is my collage project. And the language used in this project is MatLab."
  },
  {
    id: uuidv4(),
    title: "Integrated Library management system",
    content: "This software is made for the Library of Bhandarkars' Arts & Science College to keep track of number of students coming to library and attendance where registered by scanning the collage ID. this project done using ASP.NET."
  },
  {
    id: uuidv4(),
    title: "Hotel management system",
    content: "This project takes online orders for the food and keep track of it and also manages the billing of the restaurant, this project done as collage project using HTML, CSS, JavaScript and PHP."
  },
  {
    id: uuidv4(),
    title: "Steganography application",
    content: "This application is developed in android what it does is encrypt the text into an image, and it can be only decrypted with the password."
  },
]
