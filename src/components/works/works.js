import React from 'react'

import { motion } from "framer-motion";
import { Example } from '../accordian/accordian';
import '../accordian/style.css'
import page1 from '../../../public/image/page1.jpeg'
import page2 from '../../../public/image/page2.jpeg'
import page3 from '../../../public/image/page3.jpeg'
import page4 from '../../../public/image/page4.jpeg'
import Social from '../Social/Social';

const Work = () => {
  /* Hero */
  const icon = {
    hidden: {
      pathLength: 0,
      fill: "rgba(80, 17, 204, 0)"
    },
    visible: {
      pathLength: 1,
      fill: "rgba(80, 17, 204, 1)"
    }
  }

  const previewImageAnimationVariants = {
    rest: {
      backgroundPositionY: "0%"
    },
    hover: {
      backgroundPositionY: "100%",
      transition: { type: "tween", ease: "linear", duration: 5 }
    }
  };
  const previewImageAnimationVariants2 = {
    rest: {
      backgroundPositionY: "0%"
    },
    hover: {
      backgroundPositionY: "100%",
      transition: { type: "tween", ease: "linear", duration: 1 }
    }
  };

  return (
    <div id="work" className="mt-6 sm:mt-6 md:mt-10 lg:mt-12">
      <div className="flex flex-col justify-center text-center w-1/2 mx-auto mt-0 lg:w-1/2 text-2xl sm:text-2xl md:text-4xl lg:text-5xl font-bold">
        <div className="text-teal-700">My
            <span className="text-gray-900"> works</span>
          <span className="text-red-600">.</span>
        </div>
        <div className="text-gray-900 text-base sm:text-base md:text-xl lg:text-xl">
          These are the some of the projects I contributed.
          </div>
      </div>

      <div className="flex flex-wrap justify-center mx-8 mb-4 w-11/12 ml-auto mr-auto">

        <div className="mt-8 md:mx-0 max-w-lg w-full md:w-1/2 lg:w-4/12 px-6 rounded-t-lg">
          <motion.a className="block rounded-lg shadow-raised border-2 border-gray-400 shadow-2xl" initial="rest" animate="rest" whileHover="hover" rel="noreferrer" href="https://reactwind.com/" target="_blank">
            <div className="rounded-t-lg border-0 border-b-0">
              <motion.div
                className="h-128 bg-cover bg-left-top rounded-lg"
                transition={{ type: "tween" }}
                variants={previewImageAnimationVariants2}
                style={
                  {
                    'backgroundImage': "url(" + page2 + ")",
                  }
                }
              />
            </div>
            <button className="w-full bg-teal-700 hover:bg-teal-800  rounded-b-lg text-white rounded-t-none py-5 font-semibold">
              Visit
            </button>
          </motion.a>
        </div>

        <div className="mt-8 md:mx-0 max-w-lg w-full md:w-1/2 lg:w-4/12 px-6 rounded-t-lg">
          <motion.a className="block rounded-lg shadow-raised border-2 border-gray-400 shadow-2xl" initial="rest" animate="rest" whileHover="hover" rel="noreferrer" href="https://www.practiskills.com/" target="_blank">
            <div className="rounded-t-lg border-0 border-b-0 rounded-lg">
              <motion.div
                className="h-128 bg-cover bg-left-top"
                transition={{ type: "tween" }}
                variants={previewImageAnimationVariants}
                style={
                  {
                    'backgroundImage': "url(" + page3 + ")",
                  }
                }
              />
            </div>
            <button className="w-full bg-teal-700 hover:bg-teal-800  rounded-b-lg text-white rounded-t-none py-5 font-semibold">
              Visit
              </button>
          </motion.a>
        </div>

        {/* <div className="mt-8 md:mx-0 max-w-lg w-full md:w-1/2 lg:w-4/12 px-6 rounded-t-lg">
          <motion.a className="block rounded-lg shadow-raised border-2 border-gray-400 shadow-2xl" initial="rest" animate="rest" whileHover="hover" href="https://privjs.com/" target="_blank">
            <div className="rounded-t-lg border-0 border-b-0">
              <motion.div
                className="h-128 bg-cover bg-left-top rounded-lg"
                transition={{ type: "tween" }}
                variants={previewImageAnimationVariants}
                style={
                  {
                    'backgroundImage': "url(" + page1 + ")",
                  }
                }
              />
            </div>
            <button className="w-full bg-teal-700 hover:bg-teal-800 rounded-b-lg text-white rounded-t-none py-5 font-semibold">
              Visit
              </button>
          </motion.a>
        </div> */}

        <div className="mt-8 md:mx-0 max-w-lg w-full md:w-1/2 lg:w-4/12 px-6 rounded-t-lg">
          <motion.a className="block rounded-lg shadow-raised border-2 border-gray-400 shadow-2xl" initial="rest" animate="rest" whileHover="hover" rel="noreferrer" href="https://dndtailwind.netlify.app/" target="_blank">
            <div className="rounded-t-lg border-0 border-b-0">
              <motion.div
                className="h-128 bg-cover bg-left-top rounded-lg"
                transition={{ type: "tween" }}
                variants={previewImageAnimationVariants2}
                style={
                  {
                    'backgroundImage': "url(" + page4 + ")",
                  }
                }
              />
            </div>
            <button className="w-full bg-teal-700 hover:bg-teal-800 rounded-b-lg text-white rounded-t-none py-5 font-semibold">
              Visit
      </button>
          </motion.a>
        </div>

      </div>
      <Social />
      <div className="flex flex-col justify-center text-center pt-6 sm:pt-6 md:pt-16 lg:pt-16 w-11/12 mx-auto mt-0 mb-4">
        <div className="text-gray-900 text-xl font-semibold sm:text-xl md:text-2xl lg:text-3xl">
          Hobby projects that I worked on
        </div>
        <Example />
      </div>
    </div>
  )
}

export default Work